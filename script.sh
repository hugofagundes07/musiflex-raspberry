
install_file="./.script_installed"
running_file="./.runing"

if [ -f "$install_file" ]
then
	echo "packages ok"
else
	echo "instaling packages"

    apt install -y python3-pip
    pip3 install --upgrade pip
    pip3 install python-socketio

    # todo get version and update code
    echo "version=1" > "$install_file"
fi


touch $running_file
python3 main.py
rm $running_file
