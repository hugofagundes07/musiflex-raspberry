import os
import time
import platform
import socketio
import subprocess
import uuid 
import sys
# from uuid import getnode as get_mac
import threading
import netifaces
import logging
import json, requests
import atexit
from models.all import IHM2
from requests import get

with open('data.json') as json_file:
    environment = json.load(json_file)

ticketGate = None
API_URL = 'https://djxc2hjnq9xok.cloudfront.net'
logging.basicConfig(filename='principal.log',level=logging.DEBUG)
logger = logging

formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
handler = logging.FileHandler('principal.log', mode='w')
handler.setFormatter(formatter)
screen_handler = logging.StreamHandler(stream=sys.stdout)
screen_handler.setFormatter(formatter)
logger = logging.getLogger('main.musiflex')
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)
logger.addHandler(screen_handler)

myUuid = (':'.join(['{:02x}'.format((uuid.getnode() >> myUuid) & 0xff) for myUuid in range(0,8*6,8)][::-1]))
if platform.system() == "Linux":
    myUuid = netifaces.ifaddresses('eth0')[netifaces.AF_LINK][0]['addr']

sio = socketio.Client()
ip = get('https://api.ipify.org').text
authorizedPeople = [] # pessoas autorizadas na unidade
addedAuthorizedPeople = [] # pessoas que foram adicionadas na lista

@sio.event
def connect():
    ticketGate.message("    MUSIFLEX       CONECTADO    ")
    print('Connection established')
    logger.info('Connection established')
    data = {"uuid": myUuid, 'ip': ip, "platform": platform.system()}
    sio.emit('ticketGateSystemConnect', data)

# Recebe os dados do servidor
# - uuid
# - authorizedPeople
@sio.event
def ticketGateConnectionData(data):
    global authorizedPeople
    global addedAuthorizedPeople

    if 'uuid' in data and data['uuid'] == myUuid:
        print("Data received from server")
        logger.info("Data received from server")
        if 'authorizedPeople' in data:
            authorizedPeople = data['authorizedPeople']
        else:
            authorizedPeople = []
        authorizedPeople = authorizedPeople + addedAuthorizedPeople
        sio.emit('ticketGateDataReceived', {'uuid': myUuid})

@sio.event
def ticketGateConfigure(data):
    if 'uuid' in data and data['uuid'] == myUuid:
        ticketGate.configure(data['way'], data['free'])

@sio.event
def ticketGateFree(data):
    if 'uuid' in data and data['uuid'] == myUuid:
        ticketGate.free()

@sio.event
def ticketGateBlock(data):
    if 'uuid' in data and data['uuid'] == myUuid:
        ticketGate.block()

@sio.event
def ticketGateMessage(data):
    if 'uuid' in data and data['uuid'] == myUuid:
        ticketGate.message(data['message'])

@sio.event
def ticketGateReceivePerson(data):
    global authorizedPeople
    global addedAuthorizedPeople

    if 'uuid' in data and data['uuid'] == myUuid:
        print("Person received : " + str(data))
        logger.info("Person received : " + str(data))
        if 'person' not in data:
            ticketGate.block()
        else:
            person = data['person']
            authorizedPeople = authorizedPeople + [person]
            addedAuthorizedPeople = addedAuthorizedPeople + [person]
            free(person)

@sio.event
def disconnect():
    ticketGate.message("Buscando serviço...")
    print('Disconnected from server')
    logger.info('Disconnected from server')

def listenCode(code):
    global authorizedPeople

    print(len(authorizedPeople))
    logger.info(len(authorizedPeople))
    person = next((item for item in authorizedPeople if 'bar_code' in item and str(item['bar_code']) == str(code)), None)
    
    if person == None:
        print("Nobody authorized yet, lets search")
        logger.info("Nobody authorized yet, lets search")
        searchPerson(code)
    else:
        free(person)
        if 'name' in person:
            print("Person authorized : " + person['name'])
            logger.info("Person authorized : " + person['name'])

def free(person):
    th = threading.Thread(target=registrate, args=(person['id'], ))
    th.start()
    ticketGate.free({"secondLine": person['name']})

def registrate(id):
    # try:
    #     serverConnect()
    #     sio.emit("ticketGateRegistrate", {'uuid': myUuid, 'person_id': id})
    # except Exception as e:
    #     print("Failed to registrate - " + str(e))
    #     logger.info("Failed to registrate - " + str(e))
    # 85341784
    try:
        res = requests.post(API_URL + "/public-ticket-gates/registrate", data=({'uuid': myUuid, 'person_id': id}))
        json = res.json()
        if res.status_code >= 400:
            if 'error' in json:
                raise Exception(str(json['error']))
            else:
                raise Exception("Failed to registrate")
        else:
            if 'event' in json and 'data' in json:
                sio.emit(json['event'], json['data'])
    except Exception as e:
        print("Registrate fail - " + str(e))
        logger.info("Registrate fail - " + str(e))
        time.sleep(5)
        registrate(id)

def searchPerson(code):
    global authorizedPeople
    global addedAuthorizedPeople

    print("Procura pessoa : " + str(code))
    logger.info("Procura pessoa : " + str(code))
    ticketGate.message("     AGUARDE    ")

    try:
        sio.emit('ticketGateAskPerson', {'bar_code': code, 'uuid': myUuid})
    except Exception as e:
        print("Failed to emit - " + str(e))
        logger.info("Failed to emit - " + str(e))
        ticketGate.message("     CONEXAO        PERDIDA     ")
    
def loadTicketGate():
    global ticketGate 

    if environment["ticketGate"]["model"] == "IHM2":
        ticketGate = IHM2(environment["ticketGate"]["IP"], environment["ticketGate"]["PORT"])
    else:
        ticketGate = IHM2(environment["ticketGate"]["IP"], environment["ticketGate"]["PORT"])
    
    ticketGate.connect()

def serverConnect():
    print("Server status - " + str(sio.connected))
    logger.info("Server status - " + str(sio.connected))
    while not sio.connected:
        try:
            sio.connect(API_URL)
            print("Server status - " + str(sio.connected))
            logger.info("Server status - " + str(sio.connected))
        except Exception as e:
            print("failed to connect to server - " + str(e))
            logger.info("failed to connect to server - " + str(e))
            time.sleep(2)

def main():
    loadTicketGate()

    ticketGate.message("Buscando serviço")

    code = ""
    while True:
        serverConnect()
        code = input('Waiting for code...\n')
        
        try:
            listenCode(code)
        except Exception as e:
            print("failed to listen - " + str(e))
            logger.info("failed to listen - " + str(e))

def exit_handler():
    global ticketGate 
    ticketGate.close()

atexit.register(exit_handler)

main()

print("Finishing server")
logger.info("Finishing server")
