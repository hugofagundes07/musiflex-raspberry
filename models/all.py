import socket
import time
import json
import sys
import threading
import logging

with open('data.json') as json_file:
    environment = json.load(json_file)

toConnect = environment['toConnect']

logging.basicConfig(filename='principal.log',level=logging.DEBUG)
logger = logging
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
handler = logging.FileHandler('principal.log', mode='w')
handler.setFormatter(formatter)
screen_handler = logging.StreamHandler(stream=sys.stdout)
screen_handler.setFormatter(formatter)
logger = logging.getLogger('all.models.musiflex')
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)
logger.addHandler(screen_handler)

char13 = "\r"

class Catraca:
    ip = ""
    port = ""
    connection = None
    sock = None
    my_mutex = threading.Lock()

    def __init__(self, ip, port):
        self.ip = ip
        self.port = int(port)
    def connect(self):
        if toConnect:
            while self.sock == None:
                try:
                    print("Trying to connect - " + str(self.ip) + ":" + str(self.port))
                    logger.info("Trying to connect - " + str(self.ip) + ":" + str(self.port))
                    self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.sock.settimeout(5)
                    self.sock.connect((self.ip, self.port))
                    self.sock.settimeout(None)
                    print("Catraca.connect")
                    logger.info("Catraca.connect")
                except Exception as e:
                    print("Failed to connect - " + str(e))
                    logger.info("Failed to connect - " + str(e))
                    self.close()
                    time.sleep(2)
    def free(self):
        print("Catraca.free")
        logger.info("Catraca.free")
    def block(self):
        print("Catraca.block")
        logger.info("Catraca.block")
    def message(self, message):
        print("Catraca.message : " + message)
        logger.info("Catraca.message : " + message)
    def close(self):
        if toConnect:
            try:
                if self.sock != None:
                    print("Closing connection")
                    logger.info("Closing connection")
                    self.sock.shutdown(socket.SHUT_WR)
                    self.sock.close()
                    self.sock = None
                    print("Connection closed")
                    logger.info("Connection closed")
            except Exception as e:
                self.sock = None
                print("Failed to close - " + str(e))
                logger.info("Failed to close - " + str(e))
    def send(self, data):
        if self.sock == None:
            print("Not connected to send - " + str(data))
            logger.info("Not connected to send - " + str(data))
            self.connect()
        else:
            th = threading.Thread(target=self.asyncSend, args=(data, ))
            th.start()
    def asyncSend(self, data):
        if toConnect:
            self.my_mutex.acquire()
            
            try:
                print("send - " + str(data))
                logger.info("send - " + str(data))
                self.sock.send(data.encode())
            except Exception as e:
                print("Failed to send - " + str(e))
                logger.info("Failed to send - " + str(e))
                self.close()
            
            self.my_mutex.release()

class IHM2(Catraca):
    def connect(self):
        super().connect()

        lines = [
            "    MUSIFLEX    ",
            "                "
        ]

        data = "01;17;05;" + lines[0] + ";" + lines[1] + char13
        return self.send(data)

    def configure(self, SENTIDO_ENTRADA = 'E', LIBERACAO = '0'):
        data = "01;07;0;I;0;I;0;I;0;I;" + str(SENTIDO_ENTRADA) + ";1;10;281328;0;01;0;0;0;E;05;0;0;0;05;" + str(LIBERACAO) + ";0;I;0;0;0;I" + char13
        return self.send(data)

    def free(self, data = None):
        super().free()

        if data != None and 'firstLine' in data:
            firstLine = data['firstLine'][:16]
        else:
            firstLine = " Acesso Liberado"
        if data != None and 'secondLine' in data:
            secondLine = data['secondLine'][:16]
        else:
            secondLine = " SIGA EM FRENTE "

        data = "01;02;I;05;" + firstLine + ";" + secondLine + char13
        return self.send(data)
        
        
    def block(self):
        super().block()

        lines = [
            "    PROCURE A    ",
            "    RECEPCAO    "
        ]

        data = "01;03;05;" + lines[0] + ";" + lines[1] + char13
        return self.send(data)
        
    def message(self, message):
        super().message(message)

        lines = [
            message[:16],
            message[16:],
        ]

        data = "01;17;10;" + lines[0] + ";" + lines[1] + char13
        return self.send(data)
        
    
    
